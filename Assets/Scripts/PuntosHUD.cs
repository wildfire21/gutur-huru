﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntosHUD : MonoBehaviour
{
    public int puntos;
    public Text totalPuntos;
    void Start()
    {
        totalPuntos = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        puntos = GameManager.gameManager.puntuacion;
        Display();
    }

    private void Display()
    {
        totalPuntos.text = puntos.ToString();
    }

}
