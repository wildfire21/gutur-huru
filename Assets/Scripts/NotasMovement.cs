﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotasMovement : MonoBehaviour
{
    Vector3 position;
    public float speed = 2f;
    void Start()
    {
        position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 0, -(speed * Time.deltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Destroy")
        {
            Instantiate(gameObject, position, gameObject.transform.rotation);
            Destroy(gameObject);
        }
    }
}
