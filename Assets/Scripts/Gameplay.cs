﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    Vector3 position;
    public bool destroyed = false;
    void Start()
    {
        position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        destroyed = false;
        if(destroyed) Instantiate(gameObject, position, gameObject.transform.rotation);
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.A) && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Space)) && other.tag == "NotaCuerda1")
        {
            Destroy(other.gameObject);
            destroyed = true;
            GameManager.gameManager.puntuacion +=1;
        }

        else if (Input.GetKey(KeyCode.S) && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Space)) && other.tag == "NotaCuerda2")
        {
            Destroy(other.gameObject);
            destroyed = true;
            ++GameManager.gameManager.puntuacion;
        }

        else if (Input.GetKey(KeyCode.D) && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Space)) && other.tag == "NotaCuerda3")
        {
            Destroy(other.gameObject);
            destroyed = true;
            ++GameManager.gameManager.puntuacion;
        }

        else if (Input.GetKey(KeyCode.F) && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Space)) && other.tag == "NotaCuerda4")
        {
            Destroy(other.gameObject);
            destroyed = true;
            ++GameManager.gameManager.puntuacion;
        }

        else if (Input.GetKey(KeyCode.G) && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyUp(KeyCode.Space)) && other.tag == "NotaCuerda5")
        {
            Destroy(other.gameObject);
            destroyed = true;
            ++GameManager.gameManager.puntuacion;
        }
    }
}
