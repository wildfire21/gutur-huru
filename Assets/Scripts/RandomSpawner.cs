﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public GameObject notas;
    public Vector3 position = new Vector3(-3.43f, 0f, 54.42f);

    public int isRunning = 1;
    public bool stop = false;
    public int numberOfSeconds;
    public int timer;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isRunning == 1)
        {
            StartCoroutine(Wait());
        }
    }

    public IEnumerator Wait()
    {
        numberOfSeconds = Random.Range(3, 9);
        isRunning = 0;
        yield return new WaitForSeconds(numberOfSeconds);
        Instantiate(notas, position, notas.transform.rotation);
        isRunning = 1;
    }
}
